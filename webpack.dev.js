const Merge = require('webpack-merge');
const CommonConfig = require('./webpack.common.js');
const webpack = require('webpack');

const port = 5000;

module.exports = Merge(CommonConfig, {
  devtool: 'inline-source-map',
  mode: 'development',
  plugins: [
    new webpack.DefinePlugin({
      API_URL: JSON.stringify(''),
      ASSETS_URL: JSON.stringify('')
    })
  ],
  devServer: {
    inline: true,
    quiet: false,
    contentBase: './dist',
    port,
    stats: 'minimal',
    historyApiFallback: true,
    compress: true,
    open: true
  },
});
