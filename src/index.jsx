import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import indexRoutes from './routes/index';
import '../node_modules/flexboxgrid/dist/flexboxgrid.min.css';

import fontelloFonts from './assets/fonts/fontello/_fontello.scss';
import bariolFonts from './assets/fonts/bariol/_bariol.scss';
import fontAvenir from './assets/fonts/avenir/avenir.scss';

require('normalize.css');

window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      {indexRoutes.map(prop => (
        <Route path={prop.path} component={prop.component} key={prop.path} />
      ))}
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
);
