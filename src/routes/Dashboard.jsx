// core components/views
import DashboardPage from '../views/Dashboard/Dashboard';
import LoginPage from '../views/Login/Login';
import ConsultationsFormOpenedPage from '../views/ConsultationsFormOpened/ConsultationsFormOpened';

const dashboardRoutes = [
  {
    path: '/dashboard',
    sidebarName: 'Dashboard',
    navbarName: 'Elma Dashboard',
    icon: 'icon-elma-logo',
    exact: true,
    component: DashboardPage
  },
  {
    path: '/login',
    sidebarName: 'Login',
    navbarName: 'Login - View',
    icon: 'icon-elma-logo',
    exact: true,
    component: LoginPage
  },
  {
    path: '/consultations-form-opened',
    sidebarName: 'Consultations Form Opened',
    navbarName: 'Consultations Form Opened - View',
    icon: 'icon-elma-logo',
    exact: true,
    component: ConsultationsFormOpenedPage
  },
  {
    notFound: true,
    path: '/',
    sidebarName: 'NotFound',
    navbarName: 'Page NotFound',
    component: DashboardPage
  },
  {
    redirect: true,
    path: '/',
    to: '/dashboard',
    navbarName: 'Elma Dashboard'
  }
];

export default dashboardRoutes;
