import React, { Fragment, Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import '../../modules/scss/style.scss';

import logoHome from '../../assets/icon-home.svg';
import logoPatients from '../../assets/icon-patients.svg';
import logoProfile from '../../assets/icon-profile.svg';

import {
  HeaderChatComponent,
  InputChatComponent,
  MessageBoxComponent,
  MenuItemComponent,
  SnackbarComponent,
  RadioButtonComponent,
  SelectComponent,
  VerticalMenuChatComponent,
  WrapTextComponent,
  CheckBoxComponent,
  ExpansionPanelComponent,
  CopyComponent
} from '../../modules';
import ConsultationsFormOpenedPreview from './ConsultationsFormOpened/ConsultationsFormOpenedPreview';

import './ConsultationsFormOpened.scss';

const suggestions = [
  { label: 'Afghanistan', info: { title: <p>bla bla bla</p>, onClick: () => console.log('OnClick Info') } },
  { label: 'Aland Islands' },
  { label: 'Albania' },
  { label: 'Algeria' },
  { label: 'American Samoa' },
  { label: 'Andorra' },
  { label: 'Angola' },
  { label: 'Anguilla' },
  { label: 'Antarctica' },
  { label: 'Antigua and Barbuda' },
  { label: 'Argentina' },
  { label: 'Armenia' },
  { label: 'Aruba' },
  { label: 'Australia' },
  { label: 'Austria' },
  { label: 'Azerbaijan' },
  { label: 'Bahamas' },
  { label: 'Bahrain' },
  { label: 'Bangladesh' },
  { label: 'Barbados' },
  { label: 'Belarus' },
  { label: 'Belgium' },
  { label: 'Belize' },
  { label: 'Benin' },
  { label: 'Bermuda' },
  { label: 'Bhutan' },
  { label: 'Bolivia, Plurinational State of' },
  { label: 'Bonaire, Sint Eustatius and Saba' },
  { label: 'Bosnia and Herzegovina' },
  { label: 'Botswana' },
  { label: 'Bouvet Island' },
  { label: 'Brazil' },
  { label: 'British Indian Ocean Territory' },
  { label: 'Brunei Darussalam' },
].map(suggestion => ({
  value: suggestion.label,
  label: suggestion.label,
  info: suggestion.info,
}));

class ConsultationsFormOpenedPage extends Component {
  state = {
    open: false,
    SnackBarOpen: false,
    SnackBarVertical: 'bottom',
    SnackBarHorizontal: 'center',
    SnackBarText: '',
    selectedOptions: [],
    selectedOptionsSingle: ''
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleSetOption = (selectedOptionsSingle) => {
    this.setState({ selectedOptionsSingle });
    console.log(`Option selected:`, selectedOptionsSingle);
  };

  handleSetMultiOptions = (selectedOptions) => {
    this.setState({ selectedOptions });
    console.log(`Option selected:`, selectedOptions);
  };

  handleClickViedo = () => {
    console.log('handleClickViedo');
  };

  handleClickOption1 = (id) => {
    console.log(`handleClickOption1 ${id}`);
  };

  handleClickOption2 = (id) => {
    console.log(`handleClickOption2 ${id}`);
  };

  handleClickOpenSnackBar = (state) => {
    this.setState({ SnackBarOpen: true, ...state });
  };

  handleCloseSnackBar = () => {
    this.setState({ SnackBarOpen: false });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const {
      open, SnackBarOpen, SnackBarVertical, SnackBarHorizontal, SnackBarText, selectedOptions, selectedOptionsSingle
    } = this.state;
    const dummyText = (new Array(20)).fill('Lorem ipsum dolor.');
    const paragraphs = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse feugiat massa urna, sit amet aliquet orci imperdiet ac. Praesent vitae nisl nec elit tincidunt mollis vel eu augue. Cras aliquam auctor dolor, vel consequat metus condimentum ac. Praesent imperdiet suscipit semper. Cras turpis est, pretium id cursus vel, venenatis vitae ipsum. Proin volutpat suscipit justo et lobortis. Donec nulla ex, blandit at scelerisque vitae, mattis in massa.';
    const date = '01/06/2018 19:35';
    const dateHeader = 'Ultima visualización 01/06/2018 19:35';

    return (
      <Fragment>
        <SnackbarComponent
          anchorOrigin={{ vertical: SnackBarVertical, horizontal: SnackBarHorizontal }}
          open={SnackBarOpen}
          onClose={this.handleCloseSnackBar}
          text={`Copiado ID: ${SnackBarText}`}
          resumeHideDuration={3000}
          autoHideDuration={3000}
        />
        <Dialog
          fullScreen
          open={open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Preview "Opened Chat". Press Esc to Close</DialogTitle>
          <DialogContent style={{ height: '100%' }}>
            <ConsultationsFormOpenedPreview />
          </DialogContent>
        </Dialog>
        <hr />
        <div className="flex f-center width-100">
          <Button className="button-component" color="primary" variant="contained" onClick={this.handleClickOpen}>
            Preview "Opened Chat"
          </Button>
        </div>
        <hr />
        <h2> Higher-Order Components </h2>
        <div className="row">
          <div className="col-xs-12 col-md-5">
            <h4> Header Chat </h4>
            <HeaderChatComponent
              status="Open"
              title="Diego Lamas"
              date={dateHeader}
              tags={
                [
                  {
                    value: 'trial',
                    type: 'error'
                  },
                  {
                    value: 'test',
                    type: 'error'
                  },
                  {
                    value: '3 consultas cerradas'
                  }
                ]
              }
              videoProps={{
                onClick: this.handleClickViedo,
                tooltipProps: {
                  title: 'Video Llamada'
                }
              }}
              menuProps={{
                onClick: () => {
                  console.log('onClick menuProps');
                },
                tooltipProps: {
                  title: 'Opciones'
                }
              }}
              items={
                [
                  <MenuItemComponent key="1" onClick={(id) => this.handleClickOption1(`f1 Traspasar consult ${id}`)}>Traspasar consulta</MenuItemComponent>,
                  <MenuItemComponent key="2" onClick={(id) => this.handleClickOption1(`f2 Cerrar consulta ${id}`)}>Cerrar consulta</MenuItemComponent>,
                  <MenuItemComponent key="3" onClick={(id) => this.handleClickOption1(`f1 Cerrar consulta sin informe  ${id}`)}>Cerrar consulta sin informe</MenuItemComponent>,
                  <MenuItemComponent key="3" onClick={(id) => this.handleClickOption1(`f1 Cancelar  ${id}`)} type="error">Cancelar</MenuItemComponent>
                ]
              }
              id="05ad2ca7-ab6f-4fdf-a3b6-5fb434e69e07"
              description={paragraphs}
              messageCounter="2"
            />
            <br/>
            <ExpansionPanelComponent
              headerChildren={
                <div className="flex f-space-between f-align-item-start width-100">
                  <div>
                    <div>
                      <p>Doctor: Jhoan Silva</p>
                      <p>Diagnostico: Diarrea crónica</p>
                    </div>
                  </div>
                  <div>
                    <p>12/12/2018 14:05</p>
                  </div>
                </div>
              }
              actionChildren={
                <p>apartado de actions</p>
              }
            >
              <h1>
                :poop:
              </h1>
            </ExpansionPanelComponent>
            <br/>
            <CopyComponent
              id="3st0-3s-un-1d-p4r4-c0p14r"
              handleCopy={(id) => {
                this.handleClickOpenSnackBar({
                  SnackBarText: id,
                  SnackBarVertical: 'bottom',
                  SnackBarHorizontal: 'center'
                });
                console.log(`copy id: ${id}`);
              }}
              idProps={{
                tooltipProps: {
                  title: 'Copiar',
                  placement: 'right'
                }
              }}
            />
            <br/>
            <WrapTextComponent title="Title" line>
              <p>a value</p>
            </WrapTextComponent>
            <br/>
            <WrapTextComponent title="Title closer to:" line together>
              <p>a value</p>
            </WrapTextComponent>
            <br/>
            <WrapTextComponent title="Title">
              <p>a value - without line</p>
            </WrapTextComponent>
          </div>
          <div className="col-xs-12 col-md-3" style={{ overflow: 'hidden' }}>
            <h4> Vertical Menu Chat </h4>
            <div className="dashboard-vertical-menu-chat">
              <VerticalMenuChatComponent>
                <div className="flex f-center vertical-menu-chat-component-bar">
                  <img className="flex f-center width-100 pointer option" src={logoHome} alt="" />
                </div>
                <div className="flex f-center vertical-menu-chat-component-bar">
                  <img className="flex f-center width-100 pointer option" src={logoPatients} alt="" />
                </div>
                <div className="flex f-center vertical-menu-chat-component-bar">
                  <img className="flex f-center width-100 pointer option" src={logoProfile} alt="" />
                </div>
              </VerticalMenuChatComponent>
            </div>
          </div>
          <div className="col-xs-12 col-md-3">
            <h4> Message Box </h4>
            <MessageBoxComponent
              text="Hey there! What's up?"
            />
            <br/>
            <MessageBoxComponent
              text="Hey there! What's up? Hey there! What's up? Hey there! What's up? Hey there! What's up?"
              date="7:30"
            />
            <br/>
            <MessageBoxComponent
              text="Hey there! What's up?"
              date="7:30"
              type="them"
            />
            <br/>
            <div style={{ display: 'block' }}>
              <MessageBoxComponent
                text="Hoy"
                type="date"
              />
            </div>
            <br/>
            <MessageBoxComponent
              text="Hey there! What's up?"
              type="me"
              date="7:34"
              status="sending"
            />
            <br/>
            <MessageBoxComponent
              text="Hey there! What's up? Hey there! What's up? Hey there! What's up? Hey there! What's up?"
              type="me"
              date="7:34"
              status="sent"
            />
            <br/>
            <MessageBoxComponent
              text="Hey there! What's up?"
              type="them"
              date="7:35"
              status="readed"
            />
          </div>
        </div>
        <hr />
        <h2> Components </h2>
        <div className="row">
          <div className="col-xs-12 col-md-5">
            <h4> Input Chat </h4>
            <InputChatComponent
              id="1"
              type="text"
              container="input"
              placeholder="Escribe tu mensaje aquí"
              multiline
              rowsMax={4}
            />
          </div>
          <div className="col-xs-12 col-md-3">
            <h4> Form Inputs </h4>
            <h6> Multi Select </h6>
            <SelectComponent
              SelectProps={{
                placeholder: 'Select multiple countries',
                onChange: this.handleSetMultiOptions,
                label: 'Multiple countries',
                value: selectedOptions,
                options: suggestions,
                isMulti: true
              }}
            />
            <br />
            <h6> Single Select </h6>
            <SelectComponent
              SelectProps={{
                placeholder: 'Select a single country',
                label: 'Single country',
                onChange: this.handleSetOption,
                value: selectedOptionsSingle,
                options: suggestions
              }}
            />
            <br />
            <h6> Radio Button </h6>
            <RadioButtonComponent
              color="primary"
            />
            <br />
            <h6> Checkbox </h6>
            <CheckBoxComponent
              color="primary"
            />
            <br />
            <br />
          </div>
        </div>
        <br />
      </Fragment>
    );
  }
}

ConsultationsFormOpenedPage.propTypes = {
};

export default ConsultationsFormOpenedPage;
