import React from 'react';
import {
  ScrollBarComponent,
  HeaderChatComponent,
  VerticalMenuComponent,
  InputChatComponent,
  MessageBoxComponent,
  MenuItemComponent,
  IconButtonComponent,
  ConsultationCardComponent,
  FilterTextComponent,
  TitleCountComponent
} from '../../../modules';
import '../../../modules/scss/style.scss';

import logoHome from '../../../assets/icon-home.svg';
import logoPatients from '../../../assets/icon-patients.svg';
import logoProfile from '../../../assets/icon-profile.svg';
import ArrowBlue from '../../../assets/arrow-blue.svg';
import { colorData } from '../../../data/colors-data';

const ConsultationsFormOpenedPreview = () => {
  const paragraphs = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse feugiat massa urna, sit amet aliquet orci imperdiet ac. Praesent vitae nisl nec elit tincidunt mollis vel eu augue. Cras aliquam auctor dolor, vel consequat metus condimentum ac. Praesent imperdiet suscipit semper. Cras turpis est, pretium id cursus vel, venenatis vitae ipsum. Proin volutpat suscipit justo et lobortis. Donec nulla ex, blandit at scelerisque vitae, mattis in massa.';
  const date = '01/06/2018 19:35';
  const consultationCardComponentStyle = {
    margin: 25
  };
  const filterTextComponentStyle = {
    margin: 25
  };
  const containerStyle = {
    backgroundColor: colorData.secondary
  };
  const messages = [
    {
      text: "Hey there! What's up?",
      date: '7:30'
    },
    {
      text: "Hey there! What's up? Hey there! What's up? Hey there! What's up? Hey there! What's up?"
    },
    {
      text: 'Hoy',
      type: 'date'
    },
    {
      text: "Hey there! What's up?",
      type: 'me',
      date: '7:34',
      status: 'sending'
    },
    {
      text: "Hey there! What's up? Hey there! What's up? Hey there! What's up?",
      type: 'me',
      date: '7:34',
      status: 'sent'
    },
    {
      text: "Hey there! What's up? Hey there! What's up? Hey there! What's up? Hey there! What's up? Hey there! What's up? Hey there! What's up?",
      type: 'them',
      date: '7:35',
      status: 'readed'
    },
    {
      text: "Hey there! What's up?",
      type: 'them',
      date: '7:35',
      status: 'readed'
    },
    {
      text: "Hey there! What's up?",
      type: 'them',
      date: '7:35',
      status: 'readed'
    },
    {
      text: "Hey there! What's up? Hey there! What's up? Hey there! What's up? Hey there! What's up? Hey there! What's up? Hey there! What's up? Hey there! What's up?",
      type: 'me',
      date: '7:35',
      status: 'readed'
    },
    {
      text: "Hey there! What's up?",
      type: 'them',
      date: '7:35',
      status: 'readed'
    },
    {
      text: "Hey there! What's up?",
      type: 'them',
      date: '7:35',
      status: 'readed'
    },
    {
      text: "Hey there! What's up?",
      type: 'them',
      date: '7:35',
      status: 'readed'
    },
    {
      text: "Hey there! What's up?",
      type: 'them',
      date: '7:35',
      status: 'readed'
    }
  ];

  const dateHeader = 'Ultima visualización 01/06/2018 19:35';
  return (
    <div className="flex height-100">
      <div className="height-100" style={{ width: 89 }}>
        <VerticalMenuComponent>
          <div className="flex f-center vertical-menu-component-bar">
            <img className="flex f-center width-100 pointer option" src={logoHome} alt="" />
          </div>
          <div className="flex f-center vertical-menu-component-bar">
            <img className="flex f-center width-100 pointer option" src={logoPatients} alt="" />
          </div>
          <div className="flex f-center vertical-menu-component-bar">
            <img className="flex f-center width-100 pointer option" src={logoProfile} alt="" />
          </div>
        </VerticalMenuComponent>
      </div>
      <div className="row width-100 height-100">
        <div className="col-xs-4" style={{ padding: '10px 0px 25px 0px', overflow: 'hidden' }}>
          <div className="flex f-center width-100">
            <div className="flex f-center width-80">
              <FilterTextComponent
                text="Buscar"
              />
            </div>
          </div>
          <ScrollBarComponent style={{ padding: '0px 25px 0px 25px' }}>
            <div style={filterTextComponentStyle}>
              <TitleCountComponent
                text="Proximas Consultas"
                counter={1}
              >
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="1"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
              </TitleCountComponent>
            </div>
            <br />
            <div style={filterTextComponentStyle}>
              <TitleCountComponent
                text="Consultas Abiertas"
                counter={65}
              >
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="12"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="dsfsf008e-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                    active
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="123"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="84f0asef-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="1234"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="dfaef09e-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="12345"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="dfa7fad5f-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="123456"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="0890q0sdf-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="1234567"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="a0df90-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="12345678"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="05ad2ca7-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
              </TitleCountComponent>
            </div>
          </ScrollBarComponent>
        </div>
        <div className="col-xs-8 flex column" style={{ padding: '0' }} >
          <div className="scrollable-section width-100">
            <div className="col-xs-12" style={{ padding: '0' }}>
              <HeaderChatComponent
                status="Open"
                title="Diego Lamas"
                date={dateHeader}
                tags={
                  [
                    {
                      value: 'trial',
                      type: 'error'
                    },
                    {
                      value: 'test',
                      type: 'error'
                    },
                    {
                      value: '3 consultas cerradas'
                    }
                  ]
                }
                videoProps={{
                  onClick: (id) => console.log(`handleClickViedo  ${id}`),
                  tooltipProps: {
                    title: 'Video Llamada'
                  }
                }}
                menuProps={{
                  onClick: () => {
                    console.log('onClick menuProps');
                  },
                  tooltipProps: {
                    title: 'Opciones'
                  }
                }}
                items={
                  [
                    <MenuItemComponent key="1" onClick={(id) => console.log(`f1 Profile ${id}`)}>Traspasar consulta</MenuItemComponent>,
                    <MenuItemComponent key="2" onClick={(id) => console.log(`f2 account ${id}`)}>Cerrar consulta</MenuItemComponent>,
                    <MenuItemComponent key="3" onClick={(id) => console.log(`f1 Logout  ${id}`)}>Cerrar consulta sin informe</MenuItemComponent>,
                    <MenuItemComponent key="3" onClick={(id) => console.log(`f1 Logout  ${id}`)} type="error">Cancelar</MenuItemComponent>
                  ]
                }
                id="05ad2ca7-ab6f-4fdf-a3b6-5fb434e69e07"
              />
            </div>
            <div className="width-100 scrollable-section margin-b-5p">
              <ScrollBarComponent className="width-100 margin-tb-10p">
                <div className="padding-20p">
                  {messages.map((message) => {
                    let messageClassName = null;
                    switch (message.type) {
                      case 'me':
                        messageClassName = 'col-xs-offset-4 col-xs-8';
                        break;
                      case 'them':
                        messageClassName = 'col-xs-8';
                        break;

                      default:
                        messageClassName = 'col-xs-12';
                        break;
                    }
                    return (
                      <div className={messageClassName}>
                        <MessageBoxComponent
                          text={message.text}
                          type={message.type}
                          date={message.date}
                          status={message.status}
                        />
                        <br />
                      </div>
                    );
                  })}
                </div>
              </ScrollBarComponent>
            </div>
            <div className="row">
              <div className="col-xs-11">
                <InputChatComponent
                  id="1"
                  type="text"
                  container="input"
                  placeholder="Escribe tu mensaje aquí"
                  multiline
                  rowsMax={4}
                />
              </div>
              <div className="col-xs-1">
                <IconButtonComponent>
                  <img src={ArrowBlue} alt="" />
                </IconButtonComponent>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ConsultationsFormOpenedPreview;
