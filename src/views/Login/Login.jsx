import React from 'react';

import { ButtonComponent, TextComponent } from '../../modules';
import { textData } from '../../data';

const LoginPage = () => (
  <div>
    Login Components
    <hr />
    <div className="row">
      <div className="col-xs-12">
        <div className="box">Inputs</div>
        <br />
        <TextComponent id="1" text={textData.LOGIN.USER} type="email" />
        <br />
        <TextComponent id="2" text={textData.LOGIN.USER} type="email" error helperText="Error!" />
        <br />
        <TextComponent id="3" text={textData.LOGIN.USER} type="email" helperText="Helper Text!" />
        <br />
        <TextComponent id="4" text={textData.LOGIN.PASSWORD} type="password" />
      </div>
      <hr />
      <div className="col-xs-12">
        <div className="box">Buttons</div>
        <br />
        <ButtonComponent text={textData.LOGIN.ENTER} />
      </div>
    </div>
  </div>
);

export default LoginPage;
