import React from 'react';
import {
  ScrollBarComponent,
  ConsultationCardComponent,
  VerticalMenuComponent,
  FilterTextComponent,
  TitleCountComponent
} from '../../../modules';
import '../../../modules/scss/style.scss';

import logoHome from '../../../assets/icon-home.svg';
import logoPatients from '../../../assets/icon-patients.svg';
import logoProfile from '../../../assets/icon-profile.svg';
import { colorData } from '../../../data/colors-data';

const ConsultationPreview = () => {
  const paragraphs = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse feugiat massa urna, sit amet aliquet orci imperdiet ac. Praesent vitae nisl nec elit tincidunt mollis vel eu augue. Cras aliquam auctor dolor, vel consequat metus condimentum ac. Praesent imperdiet suscipit semper. Cras turpis est, pretium id cursus vel, venenatis vitae ipsum. Proin volutpat suscipit justo et lobortis. Donec nulla ex, blandit at scelerisque vitae, mattis in massa.';
  const date = '01/06/2018 19:35';
  const consultationCardComponentStyle = {
    margin: 25
  };
  const filterTextComponentStyle = {
    margin: 25
  };
  const containerStyle = {
    backgroundColor: colorData.secondary
  };
  return (
    <div className="flex height-100">
      <div className="height-100" style={{ width: 89 }}>
        <VerticalMenuComponent>
          <div className="flex f-center vertical-menu-component-bar">
            <img className="flex f-center width-100 pointer option" src={logoHome} alt="" />
          </div>
          <div className="flex f-center vertical-menu-component-bar">
            <img className="flex f-center width-100 pointer option" src={logoPatients} alt="" />
          </div>
          <div className="flex f-center vertical-menu-component-bar">
            <img className="flex f-center width-100 pointer option" src={logoProfile} alt="" />
          </div>
        </VerticalMenuComponent>
      </div>
      <div className="row width-100 height-100">
        <div className="col-xs-4" style={{ padding: '10px 0px 25px 0px', overflow: 'hidden' }}>
          <div className="flex f-center width-100">
            <div className="flex f-center width-80">
              <FilterTextComponent
                text="Buscar"
              />
            </div>
          </div>
          <ScrollBarComponent style={{ padding: '0px 25px 0px 25px' }}>
            <div style={filterTextComponentStyle}>
              <TitleCountComponent
                text="Proximas Consultas"
                counter={1}
              >
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="1"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
              </TitleCountComponent>
            </div>
            <br />
            <div style={filterTextComponentStyle}>
              <TitleCountComponent
                text="Consultas Abiertas"
                counter={65}
              >
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="12"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="dsfsf008e-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                    active
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="123"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="84f0asef-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="1234"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="dfaef09e-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="12345"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="dfa7fad5f-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="123456"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="0890q0sdf-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="1234567"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="a0df90-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
                <div style={consultationCardComponentStyle}>
                  <ConsultationCardComponent
                    key="12345678"
                    status="Open"
                    name="Nombre1 Nombre2 Apellido1 Apellido2"
                    date={date}
                    id="05ad2ca7-ab6f-4fdf-a3b6-5fb434e69e07"
                    description={paragraphs}
                    handleClick={() => { console.log('hola mundo'); }}
                    handleCopy={(id) => { console.log(`copy id: ${id}`); }}
                  />
                </div>
              </TitleCountComponent>
            </div>
          </ScrollBarComponent>
        </div>
        <div className="col-xs-8">
          <div className="row middle-xs margin-0 height-100" style={containerStyle}>
            <img className="center-xs col-xs-12 height-20" src={`${ASSETS_URL}/assets/elma-logo-new.svg`} alt="logo" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ConsultationPreview;
