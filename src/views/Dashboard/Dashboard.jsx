import React, { Fragment, Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import logoHome from '../../assets/icon-home.svg';
import logoPatients from '../../assets/icon-patients.svg';
import logoProfile from '../../assets/icon-profile.svg';

import '../../modules/scss/style.scss';

import {
  ScrollBarComponent,
  ConsultationCardComponent,
  VerticalMenuComponent,
  FilterTextComponent,
  TitleCountComponent,
  SnackbarComponent
} from '../../modules';
import ConsultationPreview from './Consultations/ConsultationPreview';

import './Dashboard.scss';

class DashboardPage extends Component {
  state = {
    open: false,
    SnackBarOpen: false,
    SnackBarVertical: 'bottom',
    SnackBarHorizontal: 'center',
    SnackBarText: ''
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClickOpenSnackBar = (state) => {
    this.setState({ SnackBarOpen: true, ...state });
  };

  handleCloseSnackBar = () => {
    this.setState({ SnackBarOpen: false });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const {
      open, SnackBarOpen, SnackBarVertical, SnackBarHorizontal, SnackBarText
    } = this.state;
    const dummyText = (new Array(20)).fill('Lorem ipsum dolor.');
    const paragraphs = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse feugiat massa urna, sit amet aliquet orci imperdiet ac. Praesent vitae nisl nec elit tincidunt mollis vel eu augue. Cras aliquam auctor dolor, vel consequat metus condimentum ac. Praesent imperdiet suscipit semper. Cras turpis est, pretium id cursus vel, venenatis vitae ipsum. Proin volutpat suscipit justo et lobortis. Donec nulla ex, blandit at scelerisque vitae, mattis in massa.';
    const date = '01/06/2018 19:35';

    return (
      <Fragment>
        <SnackbarComponent
          anchorOrigin={{ vertical: SnackBarVertical, horizontal: SnackBarHorizontal }}
          open={SnackBarOpen}
          onClose={this.handleCloseSnackBar}
          text={`Copiado ID: ${SnackBarText}`}
          resumeHideDuration={3000}
          autoHideDuration={3000}
        />
        <Dialog
          fullScreen
          open={open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Preview "Home" (Consultations). Press Esc to Close</DialogTitle>
          <DialogContent style={{ height: '100%' }}>
            <ConsultationPreview />
          </DialogContent>
        </Dialog>
        <hr />
        <div className="flex f-center width-100">
          <Button className="button-component" color="primary" variant="contained" onClick={this.handleClickOpen}>
            Preview "Home" (Consultations)
          </Button>
        </div>
        <hr />
        <h2> Higher-Order Components </h2>
        <div className="row">
          <div className="col-xs-12 col-md-3">
            <h4> Scroll Bar </h4>
            <div className="dashboard-scrollbar">
              <ScrollBarComponent>
                { dummyText.map((item, index) => <h1 key={index}> {item} </h1>) }
              </ScrollBarComponent>
            </div>
          </div>

          <div className="col-xs-12 col-md-3">
            <h4> Title Counter </h4>
            <TitleCountComponent
              text="Proximas Consultas"
              counter={1}
            >
              <h1>A</h1>
              <h1>B</h1>
              <h1>C</h1>
            </TitleCountComponent>
            <br />
            <TitleCountComponent
              text="Consultas Abiertas"
              counter={65}
            >
              <h1>1</h1>
              <h1>2</h1>
              <h1>3</h1>
            </TitleCountComponent>
          </div>
          <div className="col-xs-12 col-md-3" style={{ overflow: 'hidden' }}>
            <h4> Vertical Menu </h4>
            <div className="dashboard-vertical-menu">
              <VerticalMenuComponent>
                <div className="flex f-center vertical-menu-component-bar">
                  <img className="flex f-center width-100 pointer option" src={logoHome} alt="" />
                </div>
                <div className="flex f-center vertical-menu-component-bar">
                  <img className="flex f-center width-100 pointer option" src={logoPatients} alt="" />
                </div>
                <div className="flex f-center vertical-menu-component-bar">
                  <img className="flex f-center width-100 pointer option" src={logoProfile} alt="" />
                </div>
              </VerticalMenuComponent>
            </div>
          </div>
        </div>
        <hr />
        <h2> Components </h2>
        <div className="row">
          <div className="col-xs-12 col-md-3">
            <h4> Consultation Card </h4>
            <h6> Consultation Card Inactive (move the mouse over the card) </h6>
            <div className="dashboard-card">
              <ConsultationCardComponent
                status="Open"
                name="Nombre1 Nombre2 Apellido1 Apellido2"
                date={date}
                id="05ad2ca7-ab6f-4fdf-a3b6-5fb434e69e07"
                description={paragraphs}
                tags={
                  [
                    {
                      value: 'trial',
                      type: 'error'
                    },
                    {
                      value: 'test',
                      type: 'error'
                    }
                  ]
                }
                handleClick={() => { console.log('hola mundo'); }}
                handleCopy={(id) => {
                  this.handleClickOpenSnackBar({
                    SnackBarText: id,
                    SnackBarVertical: 'bottom',
                    SnackBarHorizontal: 'center'
                  });
                  console.log(`copy id: ${id}`);
                }}
                messageCounter="2"
              />
            </div>
            <br />
            <h6> Consultation Card Active </h6>
            <div className="dashboard-card">
              <ConsultationCardComponent
                status="Open"
                name="Nombre1 Nombre2 Apellido1 Apellido2"
                date={date}
                id="05ad2ca7-ab6f-4fdf-a3b6-5fb434e69e07"
                description={paragraphs}
                idProps={{
                  tooltipProps: {
                    title: 'Copiar',
                    placement: 'right'
                  }
                }}
                active
              />
            </div>
          </div>
          <div className="col-xs-12 col-md-3">
            <h4> Filter Text </h4>
            <FilterTextComponent
              tooltipProps={{
                title: 'limpiar'
              }}
              text="Buscar"
            />
          </div>
        </div>
        <br />
      </Fragment>
    );
  }
}

DashboardPage.propTypes = {
};

export default DashboardPage;
