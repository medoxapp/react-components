export const textData = {
  LOGIN: {
    ENTER: 'Entrar',
    USER: 'Usuario',
    PASSWORD: 'Contraseña'
  }
};

export default textData;
