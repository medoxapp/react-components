export const colorData = {
  primary: '#11f2c5',
  secondary: '#113df2'
};

export default colorData;
