import { createMuiTheme } from '@material-ui/core/styles';

export const createTheme = (defaultTheme, themeProps) => {
  const themeObject = Object.assign(defaultTheme, themeProps);
  return createMuiTheme({
    palette: themeObject
  });
};

export default createTheme;
