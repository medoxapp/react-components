import React from 'react';
import classNames from 'classnames';
import { Switch, Route, Redirect } from 'react-router-dom';
import Sidebar from '../../modules/Sidebar/component/Sidebar';
import { Header } from '../../modules/';
import dashboardRoutes from '../../routes/Dashboard';
import logo from '../../assets/elma-logo-negative.svg';

require('../style/_main.scss');

const switchRoutes = (
  <Switch>
    {dashboardRoutes.map((prop) => {
      if (prop.redirect) {
        return <Redirect from={prop.path} to={prop.to} key={prop.path} />;
      }

      if (prop.notFound) {
        return <Route component={prop.component} key={prop.path} />;
      }
      return <Route path={prop.path} exact={prop.exact} component={prop.component} key={prop.path} />;
    })}
  </Switch>
);

class App extends React.Component {
  constructor(props) {
    super(props);

    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);

    this.state = {
      mobileOpen: window.innerWidth >= 768 && true
    };
  }

  componentDidMount() {
    window.addEventListener('resize', () => {
      const { innerWidth } = window;
      if (innerWidth <= 768) {
        this.setState({
          mobileOpen: false
        });
      } else {
        this.setState({
          mobileOpen: true
        });
      }
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.history.location.pathname !== prevProps.location.pathname) {
      this.refs.mainPanel.scrollTop = 0;
      if (this.state.mobileOpen) {
        this.setState({
          mobileOpen: true
        });
      }
    }
  }

  handleDrawerToggle() {
    console.warn(!this.state.mobileOpen);
    this.setState({
      mobileOpen: !this.state.mobileOpen
    });
  }

  render() {
    const { ...rest } = this.props;
    const { mobileOpen } = this.state;
    return (
      <React.Fragment>
        <Sidebar
          mobileOpen={mobileOpen ? 'show' : 'hide'}
          routes={dashboardRoutes}
          logoText="Elma"
          logo={logo}
          color="blue"
          {...rest}
        />
        <main
          className={classNames('mainPanel', {
            fullWidth: !mobileOpen
          })}
          style={{ overflow: 'auto' }}
          ref="mainPanel"
        >
          <Header
            routes={dashboardRoutes}
            action={this.handleDrawerToggle}
            {...rest}
          />
          <div className="content">
            <div className="container-fluid">{switchRoutes}</div>
          </div>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
