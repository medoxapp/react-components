import React from 'react';
import PropTypes from 'prop-types';

import './MessageBoxComponent.scss';
import Clock from '../../assets/clock.svg';
import Check from '../../assets/tick.svg';
import DoubleCheck from '../../assets/double-tick-readed.svg';

export const MessageBoxComponent = (props) => {
  const { text, date, status, theme, className, type, children, ...parentProps } = props;

  const ComponentClassName = [className, type !== 'input' ? 'flex f-center' : '', type ? `from-${type} text-left` : 'from-unknown text-center'].join(' ');
  const dateClassName = type === 'date' ? 'flex f-center' : type === 'input' ? 'width-100' : '';
  let icon = null;
  switch (status) {
    case 'readed':
      icon = DoubleCheck;
      break;
    case 'sent':
      icon = Check;
      break;
    case 'sending':
      icon = Clock;
      break;
    default:
      break;
  }

  return (
    <div className={dateClassName} {...parentProps}>
      <div className={ComponentClassName}>
        <span />
        <span />
        <span />
        <div className="width-100">
          {text}
          {children}
          <span className="float-right">
            { date ? <span className={`${(!type ? 'bubble-time no-icon' : 'bubble-time')} ${icon === null ? 'no-icon' : ''}`}>{date}</span> : null}
            { icon ? <img src={icon} alt="" /> : null}
          </span> 
        </div>
      </div>
    </div>
  );
};

export default MessageBoxComponent;

MessageBoxComponent.propTypes = {
  text: PropTypes.string,
  theme: PropTypes.object
};

MessageBoxComponent.defaultProps = {
  text: '',
  theme: {}
};
