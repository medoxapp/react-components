import React from 'react';
import PropTypes from 'prop-types';
import { IconButton } from '../../';

require('../style/_header.scss');

const HeaderComponent = ({ ...props }) => {
  return (
    <header>
      <h3 className="title">{props.name || 'Elma Dashboard'}</h3>
      <IconButton
        className="mobile"
        action={props.action}
        color="inherit"
        iconItem="icon-menu"
        aria="open"
      />
    </header>
  )
};

HeaderComponent.propTypes = {
  color: PropTypes.oneOf([
    'primary',
    'info',
    'success',
    'warning',
    'danger'
  ]),
  action: PropTypes.func.isRequired,
  name: PropTypes.string
};

HeaderComponent.defaultProps = {
  color: 'primary',
  name: 'Elma Dashboard'
};

export { HeaderComponent };
