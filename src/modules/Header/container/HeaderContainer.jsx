import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { HeaderComponent } from '../component/Header';

class Header extends Component {
  constructor(props) {
    super(props);

    this.name = null;
  }

  componentDidMount() {
    this.makeBrand();
  }

  componentDidUpdate() {
    this.makeBrand();
  }

  makeBrand() {
    console.warn(this.props);
    this.props.routes.map((route) => {
      if (route.path === this.props.location.pathname) {
        this.name = route.navbarName;
      }
    });
  }

  render() {
    const { ...rest } = this.props;
    return (
      <HeaderComponent
        name={this.name}
        {...rest}
      />
    );
  }
}

export { Header };

Header.propTypes = {
  routes: PropTypes.array.isRequired,
  location: PropTypes.object.isRequired
};
