import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCopy } from '@fortawesome/free-solid-svg-icons';

import './ConsultationCardComponent.scss';
import '../../scss/style.scss';

import { ChipComponent } from '../../Chip/ChipComponent';
import { BadgeComponent, TooltipComponent } from '../../HOC';
import { ConsultationCardTheme, styles } from './ConsultationCardTheme';

class ConsultationCardWithThemeComponent extends Component {
  state = {
    animation: ''
  };

  componentDidUpdate(prevProps) {
    const { active } = this.props;
    if (prevProps.active === true && !active) {
      this.setState({ animation: 'no-animation' });
    }
  }

  addAnimation = () => (this.setState({ animation: 'hover' }));
  removeAnimation = () => {
    const { active } = this.props;
    if (!active) { this.setState({ animation: 'no-animation' }); }
  }

  copyToClipboard = (e) => {
    e.stopPropagation();
    const { id, handleCopy } = this.props;
    const textField = document.createElement('textarea');
    textField.innerText = id;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
    handleCopy(id);
  }

  render() {
    const {
      name,
      date,
      id,
      description,
      badgeContent,
      status,
      tags,
      theme,
      classes,
      active,
      messageCounter,
      idProps = {},
      handleClick
    } = this.props;
    const { tooltipProps: idTooltipProps = { open: false, title: '' }, ...restIdProps } = idProps;
    const { animation } = this.state;
    const themeComponent = ConsultationCardTheme(theme);

    return (
      <MuiThemeProvider theme={themeComponent}>
        <BadgeComponent badgeContent={badgeContent} color="error">
          <Card
            className={`width-100 ${classes.card} ${!active ? animation : ''} ${active ? 'active' : ''}`}
            onMouseEnter={this.addAnimation}
            onMouseLeave={this.removeAnimation}
            onClick={handleClick}
          >
            <CardContent>
              <div className="flex f-space-between width-100">
                <div className="flex">
                  <BadgeComponent badgeContent={messageCounter} color="error">
                    <span />
                  </BadgeComponent>
                  { status ? <ChipComponent style={{ marginLeft: 15 }} label={status} /> : null }

                </div>
                <span className="consultation-card-component-date">{ date }</span>
              </div>
              <div className="width-60">
                <p className="consultation-card-component-title text-truncate"> {name} </p>
              </div>
              {
                id.length > 0 ?
                  <div className="flex width-100">
                    <p role="presentation" onClick={this.copyToClipboard} className="pointer consultation-card-component-id text-truncate margin-tb-0" >
                      {id}
                    </p>
                    {
                      document.queryCommandSupported('copy') ?
                        <div role="presentation" onClick={this.copyToClipboard} {...restIdProps} className="pointer consultation-card-component-icon margin-tb-0" style={{ display: 'table' }} >
                          <TooltipComponent {...idTooltipProps}>
                            <FontAwesomeIcon icon={faCopy} />
                          </TooltipComponent>
                        </div> :
                        null
                    }
                  </div> : null
              }
              {
                description.length > 0 ?
                  <div className="width-100">
                    <p className="consultation-card-component-description block-with-text"> {description} </p>
                  </div> : null
              }
              {
                tags.length > 0 ?
                tags.map((tag, index) => <ChipComponent key={index} style={{ marginRight: 15 }} label={tag.value} type={tag.type} />) : null
              }
            </CardContent>
          </Card>
        </BadgeComponent>
      </MuiThemeProvider>
    );
  }
}

export const ConsultationCardComponent = withStyles(styles)(ConsultationCardWithThemeComponent);

export default ConsultationCardComponent;

ConsultationCardWithThemeComponent.propTypes = {
  status: PropTypes.string,
  name: PropTypes.string,
  date: PropTypes.string,
  badgeContent: PropTypes.string,
  tags: PropTypes.array,
  messageCounter: PropTypes.string,
  id: PropTypes.string,
  description: PropTypes.string,
  active: PropTypes.bool,
  handleClick: PropTypes.func,
  handleCopy: PropTypes.func,
  classes: PropTypes.object,
  theme: PropTypes.object
};

ConsultationCardWithThemeComponent.defaultProps = {
  status: '',
  name: '',
  date: '',
  id: '',
  description: '',
  badgeContent: '',
  messageCounter: '',
  active: false,
  handleClick: () => 'handleClick',
  handleCopy: () => 'handleCopy',
  tags: [],
  classes: {},
  theme: {}
};


ConsultationCardComponent.propTypes = {
  status: PropTypes.string,
  name: PropTypes.string,
  badgeContent: PropTypes.string,
  messageCounter: PropTypes.string,
  date: PropTypes.string,
  tags: PropTypes.array,
  id: PropTypes.string,
  description: PropTypes.string,
  active: PropTypes.bool,
  handleClick: PropTypes.func,
  handleCopy: PropTypes.func,
  classes: PropTypes.object,
  theme: PropTypes.object
};

ConsultationCardComponent.defaultProps = {
  status: '',
  badgeContent: '',
  messageCounter: '',
  name: '',
  date: '',
  id: '',
  description: '',
  active: false,
  handleClick: () => 'handleClick',
  handleCopy: () => 'handleCopy',
  tags: [],
  classes: {},
  theme: {}
};
