import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVideo, faEllipsisH } from '@fortawesome/free-solid-svg-icons';

import './HeaderChatComponent.scss';
import '../../scss/style.scss';

import { ChipComponent } from '../../Chip/ChipComponent';
import { MenuComponent, IconButtonComponent, TooltipComponent } from '../../HOC';
import { HeaderChatTheme, styles } from './HeaderChatTheme';

class HeaderChatWithThemeComponent extends Component {
  state = {
    anchorEl: null
  };

  handleClickMenu = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleCloseMenu = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const {
      theme,
      items,
      date,
      title,
      tags,
      style,
      className,
      videoProps,
      menuProps
    } = this.props;
    const { tooltipProps: videoTooltipProps = { open: false, title: '' }, ...restVideoProps } = videoProps;
    const { tooltipProps: menuTooltipProps = { open: false, title: '' }, ...restMenuProps } = menuProps;
    const { anchorEl } = this.state;
    const themeComponent = HeaderChatTheme(theme);

    return (
      <MuiThemeProvider theme={themeComponent}>
        <Paper
          className={className + ' padding-20p padding-tb-10p' }
          elevation={3}
          style={style}
        >
          <div className="flex f-space-between width-100">
            <div>
              <h2 className="header-chat-component-title text-truncate margin-tb-0"> {title} </h2>
              <p className="header-chat-component-date">{date}</p>
              {
                tags.length > 0 ?
                tags.map((tag, index) => <ChipComponent key={index} style={{ marginRight: 15 }} label={tag.value} type={tag.type} />) : null
              }
            </div>
            <div>
              {
                !videoProps.hide &&
                <TooltipComponent {...videoTooltipProps}>
                  <IconButtonComponent aria-label="Delete" {...restVideoProps}>
                    <FontAwesomeIcon className="header-chat-component-icon margin-tb-0" icon={restVideoProps.icon ? videoProps.icon : faVideo} />
                  </IconButtonComponent>
                </TooltipComponent>
              }
              {
                !menuProps.hide &&
                <TooltipComponent {...menuTooltipProps}>
                  <IconButtonComponent onClick={items.length > 0 ? this.handleClickMenu : menuProps.onClick} aria-label="Delete" aria-owns={anchorEl ? 'simple-menu' : null}>
                    <FontAwesomeIcon className="header-chat-component-icon margin-tb-0" icon={restMenuProps.icon ? menuProps.icon : faEllipsisH} />
                  </IconButtonComponent>
                </TooltipComponent>
              }
              <MenuComponent
                id="simple-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={this.handleCloseMenu}
              >
                {items}
              </MenuComponent>
            </div>
          </div>
        </Paper>
      </MuiThemeProvider>
    );
  }
}

export const HeaderChatComponent = withStyles(styles)(HeaderChatWithThemeComponent);

export default HeaderChatComponent;

HeaderChatWithThemeComponent.propTypes = {
  items: PropTypes.array,
  date: PropTypes.string,
  title: PropTypes.string,
  tags: PropTypes.array,
  theme: PropTypes.object
};

HeaderChatWithThemeComponent.defaultProps = {
  items: [],
  date: '',
  title: '',
  tags: [],
  theme: {}
};


HeaderChatComponent.propTypes = {
  items: PropTypes.array,
  date: PropTypes.string,
  title: PropTypes.string,
  tags: PropTypes.array,
  theme: PropTypes.object
};

HeaderChatComponent.defaultProps = {
  items: [],
  date: '',
  title: '',
  tags: [],
  theme: {}
};
