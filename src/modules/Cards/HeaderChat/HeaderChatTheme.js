import { createTheme } from '../../../utils';
import { colorData } from '../../../data';

const HeaderChatDefaultTheme = {
  primary: {
    main: colorData.primary,
    contrastText: '#fff'
  },
  secondary: {
    main: colorData.secondary,
    contrastText: '#fff'
  }
};

export const styles = () => ({
  card: {
    minWidth: 275,
    border: '2px solid rgba(0,0,0,0)',
    borderRadius: 6,
    '&.hover, &.active': {
      animationName: 'animation-in',
      animationDuration: '0.15s',
      animationFillMode: 'forwards'
    },
    '&.no-animation': {
      animationName: 'animation-out',
      animationDuration: '0.15s',
      animationFillMode: 'forwards'
    }
  },
  '@keyframes animation-in': {
    from: { border: '2px solid rgba(0,0,0,0)' },
    to: { border: '2px solid rgba(68, 48, 242, 0.94)' }
  },
  '@keyframes animation-out': {
    from: { border: '2px solid rgba(68, 48, 242, 0.94)' },
    to: { border: '2px solid rgba(0,0,0,0)' }
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)'
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  icon: {
    margin: 0,
    display: 'block',
    fontSize: 10
  }
});

export const HeaderChatTheme = (themeProps) => {
  return createTheme(HeaderChatDefaultTheme, themeProps);
};

export default HeaderChatTheme;
