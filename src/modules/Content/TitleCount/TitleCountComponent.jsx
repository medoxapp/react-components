import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';

import './TitleCountComponent.scss';
import '../../scss/style.scss';

import { TitleCountTheme, styles } from './TitleCountTheme';

class TitleCountWithThemeComponent extends Component {
  state = {
    open: true
  }

  render() {
    const {
      text,
      counter,
      classes,
      theme,
      children
    } = this.props;
    const { open } = this.state;
    const themeComponent = TitleCountTheme(theme);

    return (
      <MuiThemeProvider theme={themeComponent}>
        <div className="text-count-component pointer" role="presentation" onClick={() => { this.setState({ open: !open }) }}>
          <span className="text-count-component margin-r-20p"> { open ? '-' : '+' } </span>
          <span className="text-count-component"> {text} </span>
          <span className="text-count-component counter"> ({counter}) </span>
        </div>
        <Collapse in={open}>
          { children }
        </Collapse>
      </MuiThemeProvider>
    );
  }
}


export const TitleCountComponent = withStyles(styles)(TitleCountWithThemeComponent);

export default TitleCountComponent;

TitleCountWithThemeComponent.propTypes = {
  text: PropTypes.string,
  counter: PropTypes.number,
  classes: PropTypes.object,
  theme: PropTypes.object
};

TitleCountWithThemeComponent.defaultProps = {
  text: '',
  counter: 0,
  classes: {},
  theme: {}
};


TitleCountComponent.propTypes = {
  text: PropTypes.string,
  counter: PropTypes.number,
  classes: PropTypes.object,
  theme: PropTypes.object
};

TitleCountComponent.defaultProps = {
  text: '',
  counter: 0,
  classes: {},
  theme: {}
};
