import React from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';

import './SnackbarComponent.scss';
import { SnackbarTheme } from './SnackbarTheme';

export const SnackbarComponent = (props) => {
  const {
    text, theme, ...parentProps
  } = props;
  const themeComponent = SnackbarTheme(theme);

  return (
    <MuiThemeProvider theme={themeComponent}>
      <Snackbar
        className="snackbar-component"
        ContentProps={{
          'aria-describedby': 'message-id'
        }}
        message={<span id="message-id" style={{ textAlign: 'center' }}>{text}</span>}
        {...parentProps}
      />
    </MuiThemeProvider>
  );
};

export default SnackbarComponent;

SnackbarComponent.propTypes = {
  text: PropTypes.string,
  theme: PropTypes.object
};

SnackbarComponent.defaultProps = {
  text: '',
  theme: {}
};
