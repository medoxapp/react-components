import { createTheme } from '../../utils';
import { colorData } from '../../data';

const SnackbarDefaultTheme = {
  primary: {
    main: colorData.primary,
    contrastText: '#fff'
  },
  secondary: {
    main: colorData.secondary,
    contrastText: '#fff'
  }
};

export const SnackbarTheme = (themeProps) => {
  return createTheme(SnackbarDefaultTheme, themeProps);
};

export default SnackbarTheme;
