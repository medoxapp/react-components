import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';

import './MenuItemComponent.scss';
import { MenuItemTheme, styles } from './MenuItemTheme';

const MenuItemWithThemeComponent = (props) => {
  const {
    theme,
    classes,
    type,
    children,
    ...parentProps
  } = props;

  const themeComponent = MenuItemTheme(theme);

  return (
    <MuiThemeProvider theme={themeComponent}>
      <MenuItem className={['paddin-lr-10p menu-item-component', 'flex', 'f-start', type].join(' ')} {...parentProps}>{children}</MenuItem>
    </MuiThemeProvider>
  );
};
export const MenuItemComponent = withStyles(styles)(MenuItemWithThemeComponent);

export default MenuItemComponent;

MenuItemWithThemeComponent.propTypes = {
  text: PropTypes.string,
  theme: PropTypes.object,
  classes: PropTypes.object.isRequired
};

MenuItemWithThemeComponent.defaultProps = {
  text: '',
  theme: {}
};

MenuItemComponent.propTypes = {
  text: PropTypes.string,
  theme: PropTypes.object
};

MenuItemComponent.defaultProps = {
  text: '',
  theme: {}
};
