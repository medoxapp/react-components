import { createTheme } from '../../../../utils';
import { colorData } from '../../../../data';

const MenuItemDefaultTheme = {
  primary: {
    main: colorData.primary,
    contrastText: '#fff',
    light: '#555',
    dark: colorData.primary
  },
  secondary: {
    main: colorData.secondary,
    contrastText: '#fff'
  }
};

export const styles = () => ({
  cssInput: {
    fontSize: 18,
    fontFamily: 'BariolRegular'
  },
  cssLabel: {
    fontSize: 18,
    fontFamily: 'BariolRegular',
    color: colorData.secondary,
    '&$cssFocused': {
      color: colorData.secondary
    }
  },
  cssFocused: {},
  cssUnderline: {
    '&:before': {
      borderBottom: `2px solid ${colorData.primary}`
    }
  }
});

export const MenuItemTheme = (themeProps) => {
  return createTheme(MenuItemDefaultTheme, themeProps);
};

export default MenuItemTheme;
