import React from 'react';
import PropTypes from 'prop-types';
import Menu from '@material-ui/core/Menu';

import './MenuComponent.scss';

export const MenuComponent = (props) => {
  const {
    children, ...parentProps
  } = props;

  return (
    <Menu
      id="simple-menu"
      className="menu-component"
      {... parentProps}
    >
      {children}
    </Menu>
  );
};

export default MenuComponent;

MenuComponent.propTypes = {
  children: PropTypes.node.isRequired,
  color: PropTypes.string,
  style: PropTypes.object,
  className: PropTypes.string
};

MenuComponent.defaultProps = {
  className: '',
  style: {},
  color: 'error'
};
