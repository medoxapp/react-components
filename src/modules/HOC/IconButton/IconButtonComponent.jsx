import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';

import './IconButtonComponent.scss';

export const IconButtonComponent = (props) => {
  const {
    children, className, childrenClassName, ...parentProps
  } = props;
  return (
    <IconButton className={`icon-button-component ${!className ? '' : className}`} {...parentProps}>
      { children }
    </IconButton>
  );
};

export default IconButtonComponent;

IconButtonComponent.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

IconButtonComponent.defaultProps = {
  className: ''
};
