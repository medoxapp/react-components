import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';

import './VerticalMenuComponent.scss';
import '../../scss/style.scss';

import { VerticalMenuTheme, styles } from './VerticalMenuTheme';
import logoDefault from '../../../assets/elma-icon.svg';

class VerticalMenuWithThemeComponent extends Component {
  state = {
    animation: ''
  };

  componentDidUpdate(prevProps) {
    const { active } = this.props;
    if (prevProps.active === true && !active) {
      this.setState({ animation: 'no-animation' });
    }
  }

  addAnimation = () => (this.setState({ animation: 'hover' }));
  removeAnimation = () => {
    const { active } = this.props;
    if (!active) { this.setState({ animation: 'no-animation' }); }
  }

  copyToClipboard = (e) => {
    e.stopPropagation();
    const { id, handleCopy } = this.props;
    const textField = document.createElement('textarea');
    textField.innerText = id;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
    handleCopy(id);
  }

  render() {
    const {
      logo,
      menuOptions,
      children,
      theme,
      classes,
      active,
      handleClick,
      ...parentProps
    } = this.props;
    const { animation } = this.state;
    const themeComponent = VerticalMenuTheme(theme);

    return (
      <MuiThemeProvider theme={themeComponent}>
        <div className="vertical-menu-component">
          <div className="flex f-center vertical-menu-component-bar logo">
            <img className="flex f-center width-100 pointer" src={logo ? logo : logoDefault} alt="" />
          </div>
          { children }
        </div>
      </MuiThemeProvider>
    );
  }
}

export const VerticalMenuComponent = withStyles(styles)(VerticalMenuWithThemeComponent);

export default VerticalMenuComponent;

VerticalMenuWithThemeComponent.propTypes = {
  status: PropTypes.string,
  name: PropTypes.string,
  date: PropTypes.string,
  id: PropTypes.string,
  description: PropTypes.string,
  active: PropTypes.bool,
  handleClick: PropTypes.func,
  handleCopy: PropTypes.func,
  classes: PropTypes.object,
  theme: PropTypes.object
};

VerticalMenuWithThemeComponent.defaultProps = {
  status: '',
  name: '',
  date: '',
  id: '',
  description: '',
  active: false,
  handleClick: () => 'handleClick',
  handleCopy: () => 'handleCopy',
  classes: {},
  theme: {}
};


VerticalMenuComponent.propTypes = {
  status: PropTypes.string,
  name: PropTypes.string,
  date: PropTypes.string,
  id: PropTypes.string,
  description: PropTypes.string,
  active: PropTypes.bool,
  handleClick: PropTypes.func,
  handleCopy: PropTypes.func,
  classes: PropTypes.object,
  theme: PropTypes.object
};

VerticalMenuComponent.defaultProps = {
  status: '',
  name: '',
  date: '',
  id: '',
  description: '',
  active: false,
  handleClick: () => 'handleClick',
  handleCopy: () => 'handleCopy',
  classes: {},
  theme: {}
};
