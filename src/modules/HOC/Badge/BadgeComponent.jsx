import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Badge from '@material-ui/core/Badge';

import './BadgeComponent.scss';

export const BadgeComponent = (props) => {
  const {
    children, className, style, badgeContent, color
  } = props;

  return (
    <Fragment>
      {
        badgeContent.length > 0 ?
          <Badge className={`badge-component ${!className ? '' : className}`} style={style} badgeContent={badgeContent} color={color}>
            { children }
          </Badge>
        :
          <div className={`badge-component ${!className ? '' : className}`} style={style}>
            { children }
          </div>
      }
    </Fragment>
  );
};

export default BadgeComponent;

BadgeComponent.propTypes = {
  children: PropTypes.node.isRequired,
  badgeContent: PropTypes.string,
  color: PropTypes.string,
  style: PropTypes.object,
  className: PropTypes.string
};

BadgeComponent.defaultProps = {
  className: '',
  style: {},
  badgeContent: '',
  color: 'error'
};
