import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';

import './VerticalMenuChatComponent.scss';
import '../../scss/style.scss';

import { VerticalMenuChatTheme, styles } from './VerticalMenuChatTheme';
import logoDefault from '../../../assets/elma-icon.svg';

class VerticalMenuChatWithThemeComponent extends Component {
  state = {
    animation: ''
  };

  componentDidUpdate(prevProps) {
    const { active } = this.props;
    if (prevProps.active === true && !active) {
      this.setState({ animation: 'no-animation' });
    }
  }

  addAnimation = () => (this.setState({ animation: 'hover' }));
  removeAnimation = () => {
    const { active } = this.props;
    if (!active) { this.setState({ animation: 'no-animation' }); }
  }

  copyToClipboard = (e) => {
    e.stopPropagation();
    const { id, handleCopy } = this.props;
    const textField = document.createElement('textarea');
    textField.innerText = id;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
    handleCopy(id);
  }

  render() {
    const {
      logo,
      menuOptions,
      children,
      theme,
      classes,
      active,
      handleClick,
      ...parentProps
    } = this.props;
    const { animation } = this.state;
    const themeComponent = VerticalMenuChatTheme(theme);

    return (
      <MuiThemeProvider theme={themeComponent}>
        <div className="vertical-menu-chat-component">
          { children }
        </div>
      </MuiThemeProvider>
    );
  }
}

export const VerticalMenuChatComponent = withStyles(styles)(VerticalMenuChatWithThemeComponent);

export default VerticalMenuChatComponent;

VerticalMenuChatWithThemeComponent.propTypes = {
  status: PropTypes.string,
  name: PropTypes.string,
  date: PropTypes.string,
  id: PropTypes.string,
  description: PropTypes.string,
  active: PropTypes.bool,
  handleClick: PropTypes.func,
  handleCopy: PropTypes.func,
  classes: PropTypes.object,
  theme: PropTypes.object
};

VerticalMenuChatWithThemeComponent.defaultProps = {
  status: '',
  name: '',
  date: '',
  id: '',
  description: '',
  active: false,
  handleClick: () => 'handleClick',
  handleCopy: () => 'handleCopy',
  classes: {},
  theme: {}
};


VerticalMenuChatComponent.propTypes = {
  status: PropTypes.string,
  name: PropTypes.string,
  date: PropTypes.string,
  id: PropTypes.string,
  description: PropTypes.string,
  active: PropTypes.bool,
  handleClick: PropTypes.func,
  handleCopy: PropTypes.func,
  classes: PropTypes.object,
  theme: PropTypes.object
};

VerticalMenuChatComponent.defaultProps = {
  status: '',
  name: '',
  date: '',
  id: '',
  description: '',
  active: false,
  handleClick: () => 'handleClick',
  handleCopy: () => 'handleCopy',
  classes: {},
  theme: {}
};
