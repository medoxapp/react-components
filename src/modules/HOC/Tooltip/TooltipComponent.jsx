import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';

import './TooltipComponent.scss';

export const TooltipComponent = (props) => {
  const {
    children, className, ...parentProps
  } = props;

  return (
    <Tooltip className={`tooltip-component ${!className ? '' : className}`} {...parentProps}>
      { children }
    </Tooltip>
  );
};

export default TooltipComponent;

TooltipComponent.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

TooltipComponent.defaultProps = {
  className: ''
};
