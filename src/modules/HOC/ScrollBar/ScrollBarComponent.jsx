import React from 'react';
import PropTypes from 'prop-types';

import './ScrollBarComponent.scss';

export const ScrollBarComponent = React.forwardRef((props, ref) => {
  const {
    children, onScroll, className, style
  } = props;

  return (
    <div className={`scrollbar-component ${!className ? '' : className}`} onScroll={onScroll} ref={ref} style={style}>
      { children }
    </div>
  );
});

export default ScrollBarComponent;

ScrollBarComponent.propTypes = {
  children: PropTypes.node.isRequired
};
