import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';

export const ExpansionPanelComponent = (props) => {
  const { children, headerChildren, actionChildren, ...restProps } = props;
  return (
    <ExpansionPanel {...restProps}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          {headerChildren}
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Divider />
          {children}
        </ExpansionPanelDetails>
        {
          actionChildren ?
          <div>
            <Divider />
            <ExpansionPanelActions>
              {actionChildren}
            </ExpansionPanelActions>
          </div>
          : null
         }
      </ExpansionPanel>
  );
};
