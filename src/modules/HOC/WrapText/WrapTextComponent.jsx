import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import './WrapTextComponent.scss';

export const WrapTextComponent = (props) => {
  const {
    children, className, childrenClassName, title, line, together, spanTitleStyle, spanContentStyle, ...parentProps
  } = props;
  return (
    <div className={`wrap-text-component ${!className ? '' : className}`} {...parentProps}>
      {
        !together ?
        <Fragment>
          <h4>{title}</h4>
          { children }
        </Fragment>
        :
        <Fragment>
          <span style={spanTitleStyle}>{title+'  '}</span>
          <span style={spanContentStyle}>{ mapChildren(children) }</span>
        </Fragment>
      }
      { line ? <hr/> : null }
    </div>
  );
};

const mapChildren = (children) => {
  if (typeof children === 'array') {
    return children.map((child) => {
      const style = {...child.props.style, display: 'inline' }
      return React.cloneElement(child, {style})
    })
  }
  const style = {...children.props.style, display: 'inline' }
  return React.cloneElement(children, {style})
}

export default WrapTextComponent;

WrapTextComponent.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

WrapTextComponent.defaultProps = {
  className: ''
};
