import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import './TextComponent.scss';
import { TextTheme, styles } from './TextTheme';

const TextWithThemeComponent = (props) => {
  const {
    text,
    theme,
    classes,
    ...parentProps
  } = props;

  const themeComponent = TextTheme(theme);

  const inputClasses = {
    input: classes.cssInput,
    underline: classes.cssUnderline
  };

  const formLabelClasses = {
    root: classes.cssLabel,
    focused: classes.cssFocused
  };

  return (
    <MuiThemeProvider theme={themeComponent}>
      <TextField
        id="standard-name"
        label={text}
        className="text-component"
        InputProps={{
          classes: inputClasses
        }}
        InputLabelProps={!parentProps.error ? { FormLabelClasses: formLabelClasses } : {}}
        margin="normal"
        {...parentProps}
      />
    </MuiThemeProvider>
  );
};
export const TextComponent = withStyles(styles)(TextWithThemeComponent);

export default TextComponent;

TextWithThemeComponent.propTypes = {
  text: PropTypes.string,
  theme: PropTypes.object,
  classes: PropTypes.object.isRequired
};

TextWithThemeComponent.defaultProps = {
  text: '',
  theme: {}
};

TextComponent.propTypes = {
  text: PropTypes.string,
  theme: PropTypes.object
};

TextComponent.defaultProps = {
  text: '',
  theme: {}
};
