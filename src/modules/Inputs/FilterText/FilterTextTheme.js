import { createTheme } from '../../../utils';
import { colorData } from '../../../data';

const FilterTextDefaultTheme = {
  primary: {
    main: colorData.secondary,
    contrastText: '#fff',
    light: '#555',
    dark: colorData.secondary
  },
  secondary: {
    main: colorData.secondary,
    contrastText: '#fff'
  }
};

export const styles = () => ({
  cssInput: {
    fontSize: 18,
    fontFamily: 'BariolRegular'
  },
  cssLabel: {
    fontSize: 18,
    fontFamily: 'BariolRegular',
    color: colorData.secondary,
    '&$cssFocused': {
      color: colorData.secondary
    }
  },
  cssFocused: {},
  cssUnderline: {
    '&:before': {
      borderBottom: `2px solid ${colorData.secondary}`
    }
  }
});

export const FilterTextTheme = (themeProps) => {
  return createTheme(FilterTextDefaultTheme, themeProps);
};

export default FilterTextTheme;
