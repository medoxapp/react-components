import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import Close from '@material-ui/icons/Close';

import { TooltipComponent } from '../../HOC';

import './FilterTextComponent.scss';
import { FilterTextTheme, styles } from './FilterTextTheme';

const FilterTextWithThemeComponent = (props) => {
  const {
    text,
    handleClickClear,
    theme,
    classes,
    tooltipProps = { open: false, title: '' },
    ...parentProps
  } = props;

  const themeComponent = FilterTextTheme(theme);

  const inputClasses = {
    input: classes.cssInput,
    underline: classes.cssUnderline
  };

  const formLabelClasses = {
    root: classes.cssLabel,
    focused: classes.cssFocused
  };

  return (
    <MuiThemeProvider theme={themeComponent}>
      <TextField
        id="standard-name"
        placeholder={text}
        className="filter-text-component"
        InputProps={{
          classes: inputClasses,
          startAdornment: (
            <InputAdornment position="start">
              <Search />
            </InputAdornment>
          ),
          endAdornment: (
            <TooltipComponent {...tooltipProps}>
              <InputAdornment position="end">
                <IconButton
                  classes={formLabelClasses}
                  onClick={handleClickClear}
                >
                  <Close />
                </IconButton>
              </InputAdornment>
            </TooltipComponent>
          )
        }}
        InputLabelProps={!parentProps.error ? { FormLabelClasses: formLabelClasses } : {}}
        margin="normal"
        {...parentProps}
      />
    </MuiThemeProvider>
  );
};
export const FilterTextComponent = withStyles(styles)(FilterTextWithThemeComponent);

export default FilterTextComponent;

FilterTextWithThemeComponent.propTypes = {
  text: PropTypes.string,
  handleClickClear: PropTypes.func,
  theme: PropTypes.object,
  classes: PropTypes.object.isRequired
};

FilterTextWithThemeComponent.defaultProps = {
  text: '',
  handleClickClear: () => { console.log('Clear!'); },
  theme: {}
};

FilterTextComponent.propTypes = {
  text: PropTypes.string,
  handleClickClear: PropTypes.func,
  theme: PropTypes.object
};

FilterTextComponent.defaultProps = {
  text: '',
  handleClickClear: () => { console.log('Clear!'); },
  theme: {}
};
