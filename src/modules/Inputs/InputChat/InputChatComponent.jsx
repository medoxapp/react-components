import React from 'react';
import PropTypes from 'prop-types';
import InputBase from '@material-ui/core/InputBase';

import './InputChatComponent.scss';
import { MessageBoxComponent } from '../../index';

export const InputChatComponent = (props) => {
  const {
    text,
    theme,
    container,
    classes,
    classNameContainer,
    ...parentProps
  } = props;

  return (
    <MessageBoxComponent type={container} className={classNameContainer}>
      <InputBase
        className="width-100"
        {...parentProps}
      />
    </MessageBoxComponent>
  );
};

export default InputChatComponent;

InputChatComponent.propTypes = {
  text: PropTypes.string,
  theme: PropTypes.object
};

InputChatComponent.defaultProps = {
  text: '',
  theme: {}
};
