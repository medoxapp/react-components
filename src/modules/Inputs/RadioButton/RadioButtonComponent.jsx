import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';

import './RadioButtonComponent.scss';
import { RadioButtonTheme, styles } from './RadioButtonTheme';

const RadioButtonWithThemeComponent = (props) => {
  const {
    theme,
    ...parentProps
  } = props;

  const themeComponent = RadioButtonTheme(theme);

  return (
    <MuiThemeProvider theme={themeComponent}>
      <Radio
        {...parentProps}
      />
    </MuiThemeProvider>
  );
};
export const RadioButtonComponent = RadioButtonWithThemeComponent;

export default RadioButtonComponent;

RadioButtonWithThemeComponent.propTypes = {
  theme: PropTypes.object,
  classes: PropTypes.object.isRequired
};

RadioButtonWithThemeComponent.defaultProps = {
  theme: {}
};

RadioButtonComponent.propTypes = {
  theme: PropTypes.object
};

RadioButtonComponent.defaultProps = {
  theme: {}
};
