import { createTheme } from '../../../utils';
import { colorData } from '../../../data';

const RadioButtonDefaultTheme = {
  primary: {
    main: colorData.primary,
    contrastText: '#fff',
    light: '#555',
    dark: colorData.primary
  },
  secondary: {
    main: colorData.secondary,
    contrastText: '#fff'
  }
};

export const RadioButtonTheme = (themeProps) => {
  return createTheme(RadioButtonDefaultTheme, themeProps);
};

export default RadioButtonTheme;
