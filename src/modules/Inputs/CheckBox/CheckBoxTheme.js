import { createTheme } from '../../../utils';
import { colorData } from '../../../data';

const CheckBoxDefaultTheme = {
  primary: {
    main: colorData.primary,
    contrastText: '#fff',
    light: '#555',
    dark: colorData.primary
  },
  secondary: {
    main: colorData.secondary,
    contrastText: '#fff'
  }
};

export const CheckBoxTheme = (themeProps) => {
  return createTheme(CheckBoxDefaultTheme, themeProps);
};

export default CheckBoxTheme;
