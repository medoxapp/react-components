import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';

import './CheckBoxComponent.scss';
import { CheckBoxTheme, styles } from './CheckBoxTheme';

const CheckBoxWithThemeComponent = (props) => {
  const {
    theme,
    ...parentProps
  } = props;

  const themeComponent = CheckBoxTheme(theme);

  return (
    <MuiThemeProvider theme={themeComponent}>
      <Checkbox
        {...parentProps}
      />
    </MuiThemeProvider>
  );
};
export const CheckBoxComponent = CheckBoxWithThemeComponent;

export default CheckBoxComponent;

CheckBoxWithThemeComponent.propTypes = {
  theme: PropTypes.object,
  classes: PropTypes.object.isRequired
};

CheckBoxWithThemeComponent.defaultProps = {
  theme: {}
};

CheckBoxComponent.propTypes = {
  theme: PropTypes.object
};

CheckBoxComponent.defaultProps = {
  theme: {}
};
