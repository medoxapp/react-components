import { emphasize } from '@material-ui/core/styles/colorManipulator';

import { createTheme } from '../../../utils';
import { colorData } from '../../../data';

const SelectDefaultTheme = {
  primary: {
    main: colorData.primary,
    contrastText: '#fff',
    light: '#555',
    dark: colorData.primary
  },
  secondary: {
    main: colorData.secondary,
    contrastText: '#fff'
  }
};

export const styles = theme => ({
  cssInput: {
    fontSize: theme.fontSize ? theme.fontSize : 18,
    fontFamily: 'BariolRegular'
  },
  cssLabel: {
    fontSize: theme.labelFontSize ? theme.labelFontSize : 24,
    fontFamily: 'BariolRegular',
    color: colorData.secondary,
    '&$cssFocused': {
      color: colorData.secondary
    }
  },
  cssFocused: {},
  cssUnderline: {
    '&:before': {
      borderBottom: `2px solid ${colorData.primary}`
    }
  },
  root: {
    flexGrow: 1,
  },
  input: {
    display: 'flex',
    padding: 0,
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    ),
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    fontSize: 18,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 18,
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing.unit * 2,
  },
});

export const SelectTheme = (themeProps) => {
  return createTheme(SelectDefaultTheme, themeProps);
};

export default SelectTheme;
