import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';

import Select from 'react-select';
import MenuItem from '@material-ui/core/MenuItem';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfo } from '@fortawesome/free-solid-svg-icons';

import { ChipComponent } from '../../Chip/ChipComponent';
import { IconButtonComponent, TooltipComponent } from '../../HOC';

import './SelectComponent.scss';
import {
  SelectTheme,
  styles
} from './SelectTheme';

const NoOptionsMessage = props => (
  <Typography
    color="textSecondary"
    className={props.selectProps.classes.noOptionsMessage}
    {...props.innerProps}
  >
    {props.children}
  </Typography>
);

const inputComponent = ({ inputRef, ...props }) => <div ref={inputRef} {...props} />;

const Control = props => (
  <TextField
    fullWidth
    InputProps={{
      inputComponent,
      // props.selectProps.classes
      classes: {
        input: props.selectProps.classes.cssInput,
        underline: props.selectProps.classes.cssUnderline
      },
      inputProps: {
        className: props.selectProps.classes.input,
        inputRef: props.innerRef,
        children: props.children,
        ...props.innerProps
      }
    }}
    {...props.selectProps.textFieldProps}
  />
);

const Option = props => (
  <MenuItem
    buttonRef={props.innerRef}
    selected={props.isFocused}
    component="div"
    style={{
      fontWeight: props.isSelected ? 500 : 400,
    }}
    {...props.innerProps}
  >
    {props.children}
  </MenuItem>
);

const Placeholder = props => (
  <Typography
    color="textSecondary"
    className={props.selectProps.classes.placeholder}
    {...props.innerProps}
  >
    {props.children}
  </Typography>
);

const SingleValue = props => (
  <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
    {props.children}
  </Typography>
);

const ValueContainer = props => <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;

const MultiValue = (props) => {
  const { data } = props;

  const infoIcon = data.info && data.info.title ? { title: data.info.title } : {};
  const onDelete = data.info && data.info.onClick !== undefined ? data.info.onClick : undefined;
  const deleteIcon = data.info && data.info.onClick !== undefined ?
    (
      <TooltipComponent {...infoIcon}>
        <FontAwesomeIcon onClick={onDelete} style={{ margin: '0px 15px 0px 0px' }} icon={faInfo} className="select-chip-component-icon-info" />
      </TooltipComponent>
    ) : undefined;
  const ChipComponentProps = { onDelete, deleteIcon };


  return (
    <ChipComponent
      style={{ margin: '5px 10px 5px 0px' }}
      className="select-chip-component"
      tabIndex={-1}
      label={props.children}
      {...ChipComponentProps}
      avatar={
        <IconButtonComponent onClick={props.removeProps.onClick} className="select-chip-component-icon">
          <span className="select-chip-component-icon-close">x</span>
        </IconButtonComponent>
      }
    />
  );
};

const Menu = props => (
  <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
    {props.children}
  </Paper>
);

const components = {
  Control,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer
};

class SelectWithThemeComponent extends React.Component {
  state = {
    single: null,
    multi: null,
  };

  handleChange = name => value => {
    this.setState({
      [name]: value,
    });
  };

  render() {
    const {
      theme,
      classes,
      SelectProps
    } = this.props;

    const formLabelClasses = {
      root: classes.cssLabel,
      focused: classes.cssFocused
    };

    const themeComponent = SelectTheme(theme);

    return (
      <MuiThemeProvider theme={themeComponent}>
        <div className={classes.root}>
          <NoSsr>
            <Select
              {...SelectProps}
              classes={classes}
              textFieldProps={{
                label: SelectProps.label,
                InputLabelProps: {
                  shrink: true,
                  FormLabelClasses: formLabelClasses
                }
              }}
              components={components}
            />
          </NoSsr>
        </div>
      </MuiThemeProvider>
    );
  }
}

export const SelectComponent = withStyles(styles)(SelectWithThemeComponent);

export default SelectComponent;

SelectWithThemeComponent.propTypes = {
  theme: PropTypes.object,
  classes: PropTypes.object.isRequired
};

SelectWithThemeComponent.defaultProps = {
  theme: {}
};

SelectComponent.propTypes = {
  theme: PropTypes.object
};

SelectComponent.defaultProps = {
  theme: {}
};
