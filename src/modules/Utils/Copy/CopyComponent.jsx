import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCopy } from '@fortawesome/free-solid-svg-icons';

import { TooltipComponent } from '../../HOC';

import './CopyComponent.scss';
import '../../scss/style.scss';

import { CopyTheme, styles } from './CopyTheme';
const CopyWithThemeComponent = (props) => {
  const {
    id,
    idProps = {},
    theme,
    style
  } = props;
  const { tooltipProps: idTooltipProps = { open: false, title: '' }, ...restIdProps } = idProps;
  const themeComponent = CopyTheme(theme);
  const copyToClipboard = (e) => {
    e.stopPropagation();
    const { id, handleCopy } = props;
    const textField = document.createElement('textarea');
    textField.innerText = id;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
    handleCopy(id);
  }

  

  return (
    <MuiThemeProvider theme={themeComponent}>
      {
        id.length > 0 ?
          <div className="flex width-100" style={style}>
            <p role="presentation" onClick={copyToClipboard} className="pointer consultation-card-component-id text-truncate margin-tb-0" >
              {id}
            </p>
            {
              document.queryCommandSupported('copy') ?
                <div role="presentation" onClick={copyToClipboard} {...restIdProps} className="pointer consultation-card-component-icon margin-tb-0" style={{ display: 'table' }} >
                  <TooltipComponent {...idTooltipProps}>
                    <FontAwesomeIcon icon={faCopy} />
                  </TooltipComponent>
                </div> :
                null
            }
          </div> : null
      }
    </MuiThemeProvider>
  );
};

export const CopyComponent = withStyles(styles)(CopyWithThemeComponent);

export default CopyComponent;

CopyWithThemeComponent.propTypes = {
  id: PropTypes.string,
  handleCopy: PropTypes.func,
  classes: PropTypes.object,
  theme: PropTypes.object
};

CopyWithThemeComponent.defaultProps = {
  id: '',
  handleCopy: () => 'handleCopy',
  classes: {},
  theme: {}
};


CopyComponent.propTypes = {
  id: PropTypes.string,
  handleCopy: PropTypes.func,
  classes: PropTypes.object,
  theme: PropTypes.object
};

CopyComponent.defaultProps = {
  id: '',
  handleCopy: () => 'handleCopy',
  classes: {},
  theme: {}
};
