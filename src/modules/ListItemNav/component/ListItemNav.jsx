import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from '../../';

require('../style/listItemNav.scss');

const ListItemNav = ({
  nameItemNav,
  iconItemNav,
  colorIconItemNav
}) => (
  <section id={nameItemNav} className="itemNav">
    {iconItemNav &&
      <Icon
        iconColor={colorIconItemNav}
        iconItem={iconItemNav}
      />
    }
    <p className="itemTextNav">{nameItemNav}</p>
  </section>
);

export { ListItemNav };

ListItemNav.propTypes = {
  nameItemNav: PropTypes.string,
  iconItemNav: PropTypes.string,
  colorIconItemNav: PropTypes.string
};

ListItemNav.defaultProps = {
  nameItemNav: null,
  iconItemNav: null,
  colorIconItemNav: null
};
