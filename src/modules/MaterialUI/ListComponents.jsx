import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';

export const ListComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <List {...restProps}>
      {children}
    </List>
  );
};

export const ListItemComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <ListItem {...restProps}>
      {children}
    </ListItem>
  );
};

export const ListItemAvatarComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <ListItemAvatar {...restProps}>
      {children}
    </ListItemAvatar>
  );
};

export const ListItemIconComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <ListItemIcon {...restProps}>
      {children}
    </ListItemIcon>
  );
};

export const ListItemSecondaryActionComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <ListItemSecondaryAction {...restProps}>
      {children}
    </ListItemSecondaryAction>
  );
};

export const ListItemTextComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <ListItemText {...restProps}>
      {children}
    </ListItemText>
  );
};
