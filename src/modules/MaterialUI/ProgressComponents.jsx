import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import CircularProgress from '@material-ui/core/CircularProgress';

export const LinearProgressComponent = (props) => {
  const { linearProgressProps } = props;
  return (
    <LinearProgress {...linearProgressProps} />
  );
};

export const CircularProgressComponent = (props) => {
  const { circularProgressProps } = props;
  return (
    <CircularProgress {...circularProgressProps} />
  );
};
