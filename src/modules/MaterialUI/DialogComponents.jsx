import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export const DialogComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <Dialog {...restProps}>
      {children}
    </Dialog>
  );
};

export const DialogActionsComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <DialogActions {...restProps}>
      {children}
    </DialogActions>
  );
};

export const DialogContentComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <DialogContent {...restProps}>
      {children}
    </DialogContent>
  );
};

export const DialogContentTextComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <DialogContentText {...restProps}>
      {children}
    </DialogContentText>
  );
};

export const DialogTitleComponent = (props) => {
  const { children, ...restProps } = props;
  return (
    <DialogTitle {...restProps}>
      {children}
    </DialogTitle>
  );
};

