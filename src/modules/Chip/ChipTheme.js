import { createTheme } from '../../utils';
import { colorData } from '../../data';

const ChipDefaultTheme = {
  primary: {
    main: colorData.primary,
    contrastText: '#fff'
  },
  secondary: {
    main: colorData.secondary,
    contrastText: '#fff'
  }
};

export const ChipTheme = (themeProps) => {
  return createTheme(ChipDefaultTheme, themeProps);
};

export default ChipTheme;
