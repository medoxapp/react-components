import React from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';

import './ChipComponent.scss';
import { ChipTheme } from './ChipTheme';

export const ChipComponent = (props) => {
  const {
    theme,
    type,
    className,
    ...parentProps
  } = props;

  const themeComponent = ChipTheme(theme);
  const childrenclassname = type === 'error' ? `chip-component error ${className}` : `chip-component ${className}`;

  return (
    <MuiThemeProvider theme={themeComponent}>
      <Chip
        className={childrenclassname}
        {...parentProps}
      />
    </MuiThemeProvider>
  );
};

export default ChipComponent;

ChipComponent.propTypes = {
  text: PropTypes.string,
  theme: PropTypes.object
};

ChipComponent.defaultProps = {
  text: '',
  theme: {}
};
