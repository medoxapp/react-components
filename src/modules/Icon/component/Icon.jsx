import React from 'react';
import PropTypes from 'prop-types';

const Icon = ({
  iconItem,
  iconColor
}) => (
  <i className={`icon ${iconColor} ${iconItem}`} />
);

export { Icon };

Icon.propTypes = {
  iconItem: PropTypes.string.isRequired,
  iconColor: PropTypes.string
};

Icon.defaultProps = {
  iconColor: 'inherit'
};
