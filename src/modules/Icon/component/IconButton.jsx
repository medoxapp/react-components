import React from 'react';
import PropTypes from 'prop-types';

const IconButton = ({
  iconItem,
  iconColor,
  className,
  action,
  aria
}) => (
  <span
    role="button"
    tabIndex={0}
    onClick={action}
    onKeyPress={action}
    className="iconButton"
  >
    <i
      aria-label={aria}
      className={`${iconColor} ${iconItem} ${className}`}
    />
  </span>
);

export { IconButton };

IconButton.propTypes = {
  iconItem: PropTypes.string.isRequired,
  iconColor: PropTypes.string,
  className: PropTypes.string,
  action: PropTypes.func.isRequired,
  aria: PropTypes.string
};

IconButton.defaultProps = {
  iconColor: 'inherit',
  className: null,
  aria: null
};
