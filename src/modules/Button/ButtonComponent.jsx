import React from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import './ButtonComponent.scss';
import { ButtonTheme } from './ButtonTheme';

export const ButtonComponent = (props) => {
  const { text, theme, children, ...parentProps } = props;

  const themeComponent = ButtonTheme(theme);

  return (
    <MuiThemeProvider theme={themeComponent}>
      <Button className="button-component" color="primary" variant="contained" {...parentProps}>
        { text }
        { children }
      </Button>
    </MuiThemeProvider>
  );
};

export default ButtonComponent;

ButtonComponent.propTypes = {
  text: PropTypes.string,
  theme: PropTypes.object
};

ButtonComponent.defaultProps = {
  text: '',
  theme: {}
};
