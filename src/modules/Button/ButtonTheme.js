import { createTheme } from '../../utils';
import { colorData } from '../../data';

const ButtonDefaultTheme = {
  primary: {
    main: colorData.primary,
    contrastText: '#fff'
  },
  secondary: {
    main: colorData.secondary,
    contrastText: '#fff'
  }
};

export const ButtonTheme = (themeProps) => {
  return createTheme(ButtonDefaultTheme, themeProps);
};

export default ButtonTheme;
