import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { ListItemNav } from '../../';

require('../style/sidebar.scss');

const Sidebar = (props) => {
  const {
    color,
    logo,
    logoText,
    routes,
    mobileOpen
  } = props;

  const brand = (
    <div className={`brand ${color}`}>
      <img src={logo} alt={logoText} className="img" />
    </div>
  );

  const ui = (
    <aside
      className={`sidebar ${color} ${mobileOpen}`}
    >
      {logo &&
        <React.Fragment>
          {brand}
        </React.Fragment>
      }
      <nav>
        {routes.map((route) => {
          if (route.redirect) return null;
          if (route.notFound) return null;
          return (
            <NavLink
              to={route.path}
              activeClassName="active"
              key={route.path}
            >
              <ListItemNav
                nameItemNav={route.sidebarName}
                iconItemNav={route.icon}
                colorIconItemNav="white"
              />
            </NavLink>
          );
        })}
      </nav>
    </aside>
  );

  return (
    <React.Fragment>
      {ui}
    </React.Fragment>
  );
};

Sidebar.propTypes = {
  color: PropTypes.string,
  logo: PropTypes.string,
  logoText: PropTypes.string,
  routes: PropTypes.array,
  mobileOpen: PropTypes.string
};

Sidebar.defaultProps = {
  color: 'blue',
  logo: null,
  logoText: null,
  routes: [],
  mobileOpen: 'show'
};

export default Sidebar;
