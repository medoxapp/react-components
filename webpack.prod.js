const Merge = require('webpack-merge');
const CommonConfig = require('./webpack.common.js');
const webpack = require('webpack');

module.exports = Merge(CommonConfig, {
  devtool: 'cheap-module-source-map',
  mode: 'production',
  plugins: [
    new webpack.DefinePlugin({
      API_URL: JSON.stringify(''),
      ASSETS_URL: JSON.stringify('/dev-elma-reactjs-components')
    })
  ]
});
