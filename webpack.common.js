const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const eslintFormatter = require('react-dev-utils/eslintFormatter');
const LiveReloadPlugin = require('webpack-livereload-plugin');

const BUILD_DIR = path.resolve(__dirname, './dist');
const APP_DIR = path.resolve(__dirname, './src');


const config = {
  entry: `${APP_DIR}/index.jsx`,
  output: {
    path: `${BUILD_DIR}`,
    filename: 'main.js',
    publicPath: ''
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      title: 'Elma Library',
      template: `${APP_DIR}/index.html`,
      hash: true
    }),
    new LiveReloadPlugin(),
    new CopyWebpackPlugin([
      {
        from: `${APP_DIR}/assets`,
        to: `${BUILD_DIR}/assets`
      }
    ])
  ],
  target: 'web',
  module: {
    rules: [
      // {
      //   test: /\.(js|jsx)$/,
      //   enforce: 'pre',
      //   use: [
      //     {
      //       options: {
      //         formatter: eslintFormatter
      //       },
      //       loader: require.resolve('eslint-loader')
      //     }
      //   ],
      //   include: APP_DIR
      // },
      {
        test: /\.(js|jsx)$/,
        include: APP_DIR,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }, {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              minimize: true,
              removeComments: true
            }
          }
        ]
      }, {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }, {
        test: /\.sass/,
        loader: 'style-loader!css-loader!sass-loader?outputStyle=expanded&indentedSyntax'
      }, {
        test: /\.scss/,
        loader: 'style-loader!css-loader!sass-loader?outputStyle=expanded'
      }, {
        test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: 'base64-inline-loader?limit=10000&name=[name].[ext]'
      }, {
        test: /\.(mp4|ogg)$/,
        loader: 'file-loader'
      }, {
        type: 'javascript/auto',
        test: /\.json$/,
        exclude: /node_modules/,
        loader: 'json-loader'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    descriptionFiles: ['package.json']
  }
};

module.exports = config;
